import { Component, OnInit, Inject } from "@angular/core";
import { ForumService } from '../service/forum.service';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute} from "@angular/router";
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import { SeoService } from "../service/seo.service";
import { DOCUMENT } from "@angular/platform-browser";


@Component({
  selector: "app-health-details",
  templateUrl: "./health-details.component.html",
  styleUrls: ["./health-details.component.scss"]
})
export class HealthDetailsComponent implements OnInit {

  public text;

  title;
  Categories;
  LastUpdate;
  ImageUrl;
  contents;
  description;

  errMess: string;
  constructor(
    private route: ActivatedRoute,
    private forumservice: ForumService,
    private titleService: Title,
    private meta: Meta,
    private seo: SeoService,
    @Inject(DOCUMENT) private document: any
  ) {
    // console.log(location.pathname);
    // console.log(location.href);
    // console.log(this.document.location.href);
  }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");
    this.forumservice.getHealth(id).subscribe(data => {
      Object.keys(data).forEach(
        key => {
          const topics = data[key];
          const health = topics.Resources.Resource;
          Object.keys(health).forEach(
            key => {
              const Section = health[key];
              this.title = Section.Title;
              this.ImageUrl = Section.ImageUrl;
              this.contents = Section.Sections.Section;
              this.description = Section.Sections.Section[0];
              console.log(this.description);
              this.seo.generateTags({
                title: this.description.Description,
                description: this.description.Description,
                image: Section.ImageUrl,
                slug: this.document.location.href
              });
            },
            errmess => (this.errMess = errmess)
          );
        },
        errmess => (this.errMess = errmess)
      );
    });

    // this.titleService.setTitle(this.title);
    // this.meta.addTag({
    //   name: this.title,
    //   // tslint:disable-next-line:max-line-length
    //   content: 'ACCA ' + this.description.Description
    // });
  }
}
