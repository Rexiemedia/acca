import { Component, OnInit } from '@angular/core';
import { flyInOut } from '../animations/app.animation';
import { SeoService } from '../service/seo.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[@flyInOut]': 'true',
    style: 'display: block;'
  },
  animations: [flyInOut()]
})
export class AboutComponent implements OnInit {
  public about = 'A church commisioned base on the vision of Lord and savoiur jesus Christ. On the initiated relationship by Christ having chosen us. He commissioned us to bring forth permanent fruit through Visionary leadership of a new world order as profounded by Him. (Jn. 15: 14 - 17; Lk. 9: 55 - 56)';

  constructor(private seo: SeoService) {}

  ngOnInit() {

    this.seo.generateTags({
      title: 'Anointed Celebrating Christ Assembly',
      description: this.about,
      image: 'https://instafire-app.firebaseapp.com/assets/meerkat.jpeg',
      slug: 'about-page'
    });
  }
}
