import { Component, OnInit, Inject } from "@angular/core";
import { SermonService } from '../service/sermon.service';
import { Sermon } from '../shared/sermon';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
// import { visibility, flyInOut, expand } from '../animations/app.animation';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import { ShareButtons } from '@ngx-share/core';
import { SeoService } from "../service/seo.service";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: 'app-sermon-details',
  templateUrl: './sermon-details.component.html',
  styleUrls: ['./sermon-details.component.scss']
})
export class SermonDetailsComponent implements OnInit {
  sermon: Sermon;
  errMess: string;
  commentIds: string;
  visibility = 'shown';

  public title;
  public author;
  public message;
  SermonId;


  url = '';
  text = '';
  imageUrl = '';

  constructor(
    private sermonservice: SermonService,
    private route: ActivatedRoute,
    public share: ShareButtons,
    private location: Location,
    private titleService: Title,
    private meta: Meta,
     private seo: SeoService,
    @Inject(DOCUMENT) private document: any
  ) {}

  ngOnInit() {
    this.sermonservice
      .getSermon(this.route.snapshot.paramMap.get('id'))
      .subscribe(res => {
        this.title = res.title;
        this.author = res.author;
        this.message = res.message;
        this.text = res.title;
        this.SermonId = res._id;

        this.seo.generateTags({
          title: res.title,
          description: res.title,
          image: ' ',
          slug: this.document.location.href
        });

        this.titleService.setTitle(this.text);
        this.meta.addTag({ name: 'description', content: res.title });
      });
  }
}
