import { Component, OnInit } from "@angular/core";
import { SermonService } from "../service/sermon.service";
import { Sermon, Testimony } from "../shared/sermon";

@Component({
  selector: "app-sermon",
  templateUrl: "./sermon.component.html",
  styleUrls: ["./sermon.component.scss"]
})
export class SermonComponent implements OnInit {
  testimony: Testimony[];
  tesMess: string;

  sermons: Sermon[];
  serMess: string;

  constructor(private sermonservice: SermonService) {}

  ngOnInit() {
    this.sermonservice.getSermones().subscribe(
      sermons => {
        this.sermons = sermons;
      },
      errmess => (this.serMess = errmess)
    );

    this.sermonservice.getTestimonies().subscribe(
      testimony => {
        this.testimony = testimony;
      },
      errmess => (this.tesMess = errmess)
    );
  }
}
