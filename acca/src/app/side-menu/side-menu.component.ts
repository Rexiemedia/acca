import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  constructor(route: ActivatedRoute) {
    route.params.subscribe(params =>
      console.log('side Activated')
    );
  }

  ngOnInit() {}
}
