import { TestBed, inject } from '@angular/core/testing';

import { PrayerService } from './prayer.service';

describe('PrayerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrayerService]
    });
  });

  it('should be created', inject([PrayerService], (service: PrayerService) => {
    expect(service).toBeTruthy();
  }));
});
