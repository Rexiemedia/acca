import { Injectable } from '@angular/core';
import { Prayer } from '../shared/feedback';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection
} from 'angularfire2/firestore';


@Injectable({
  providedIn: 'root'
})
export class PrayerService {
  private basePath = '/prayers';
  success;
  constructor(private db: AngularFirestore) {}

  submitPrayer(data: Prayer): Promise<any> {
    return this.db
      .collection<Prayer>(this.basePath)
      .add(data)
      .then(res => {
        res = this.success;
      });
  }
}
