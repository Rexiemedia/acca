import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Health, Social } from '../shared/forum';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection
} from 'angularfire2/firestore';
import { AuthService } from './auth.service';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { map } from 'rxjs-compat/operator/map';

@Injectable({
  providedIn: "root"
})
export class ForumService {
  private currentUser: firebase.User = null;
  public url;
  url2;
  Id: number;
  constructor(
    private http: HttpClient,
    private afs: AngularFirestore,
    private authService: AuthService
  ) {
    this.afs.firestore.settings({ timestampsInSnapshots: true });
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        // User is signed in.
        this.currentUser = user;
      } else {
        this.currentUser = null;
      }
    });

    this.url ="https://healthfinder.gov/FreeContent/Developer/Search.json?api_key=vygfgdvfaxyexotc&CategoryID=16";
    this.url2 = "https://healthfinder.gov/api/v2/topicsearch.json?api_key=vygfgdvfaxyexotc&TopicId=";
  }

  getHealths() {
    return this.http.get(this.url)
      .map(data => {
       const result = data;
       return result;
      });
  }
  getHealth(Id) {
    return this.http.get(this.url2 + Id);
    // .map(data => {
    //   const result = data;
    //   console.log(result);
    //   return { ...result };
    // });
  }
}
