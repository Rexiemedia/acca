import { Injectable } from '@angular/core';
import { Feedback } from '../shared/feedback';
import { AngularFirestore, AngularFirestoreDocument,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import { AuthService } from '../service/auth.service';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  private currentUser: firebase.User = null;
  private basePath = '/feedbacks';
  success;
  constructor(
    private db: AngularFirestore,
    private authService: AuthService
  ) {
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        // User is signed in.
        this.currentUser = user;
      } else {
        this.currentUser = null;
      }
    });
  }

  submitFeedback(data: Feedback): Promise<any> {
    return this.db
      .collection<Feedback>(this.basePath)
      .add(data)
      .then(res => {
        res = this.success;
      });
  }

  getFeedback(path): Observable<any[]> {
    return this.db.collection(path).valueChanges();
  }
}
