import { Injectable, OnInit } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Sermon, Testimony } from "../shared/sermon";
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection
} from "angularfire2/firestore";
import { AuthService } from "./auth.service";
import * as firebase from "firebase/app";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

@Injectable({
  providedIn: "root"
})
export class SermonService {
  private currentUser: firebase.User = null;

  constructor(private afs: AngularFirestore, private authService: AuthService) {
    this.afs.firestore.settings({ timestampsInSnapshots: true });
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        // User is signed in.
        this.currentUser = user;
      } else {
        this.currentUser = null;
      }
    });
  }

  getSermones(): Observable<Sermon[]> {
    return this.afs
      .collection<Sermon>("sermons")
      .snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as Sermon;
          const _id = action.payload.doc.id;
          return { _id, ...data };
        });
      });
  }

  getSermon(id: string): Observable<Sermon> {
    return this.afs
      .doc<Sermon>("sermons/" + id)
      .snapshotChanges()
      .map(action => {
        const data = action.payload.data() as Sermon;
        const _id = action.payload.id;
        return { _id, ...data };
      });
  }

  getFeaturedSermon(): Observable<Sermon> {
    return this.afs
      .collection<Sermon>("sermons", ref => ref.where("featured", "==", true))
      .snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as Sermon;
          const _id = parseInt(action.payload.doc.id, 10);
          return { _id, ...data };
        })[0];
      });
  }

  getComments(SermonId: string): Observable<any> {
    return this.afs
      .collection("comments", ref => ref.where("Sermonid", "==", SermonId))
      .valueChanges();
  }

  postComment(SermonId: string, comment: any): Promise<any> {
    if (this.currentUser) {
      return this.afs
        .collection("sermons")
        .doc(SermonId)
        .collection("comments")
        .add({
          author: {
            _id: this.currentUser.uid,
            firstname: this.currentUser.displayName
              ? this.currentUser.displayName
              : this.currentUser.email
          },
          rating: comment.rating,
          comment: comment.comment,
          createdAt: firebase.firestore.FieldValue.serverTimestamp(),
          updatedAt: firebase.firestore.FieldValue.serverTimestamp()
        });
    } else {
      return Promise.reject(new Error("No User Logged In!"));
    }
  }

  // Testimonie
  getTestimonies(): Observable<Testimony[]> {
    return this.afs
      .collection<Testimony>("testimonies")
      .snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as Testimony;
          const _id = action.payload.doc.id;
          return { _id, ...data };
        });
      });
  }
}
