import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FeedbackService } from "../service/feedback.service";

import { Feedback, ContactType } from "../shared/feedback";
import { flyInOut, expand } from "../animations/app.animation";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    "[@flyInOut]": "true",
    style: "display: block;"
  },
  animations: [flyInOut(), expand()]
})
export class ContactComponent implements OnInit {
  @ViewChild("fform")
  feedbackFormDirective;
  feedbackForm: FormGroup;
  feedback: Feedback;
  contactType = ContactType;
  submitted = null;
  showForm = true;

  formErrors = {
    firstname: "",
    lastname: "",
    telnum: "",
    email: ""
  };

  validationMessages = {
    firstname: {
      required: "First Name is required.",
      minlength: "First Name must be at least 3 characters long",
      maxlength: "First Name cannot be more than 25 characters long"
    },
    lastname: {
      required: "Last Name is required.",
      minlength: "Last Name must be at least 3 characters long",
      maxlength: "Last Name cannot be more than 25 characters long"
    },
    telnum: {
      required: "Tel. Number is required.",
      pattern: "Tel. number must contain only numbers."
    },
    email: {
      required: "Email is required.",
      email: "Email not in valid format"
    }
  };

  constructor(
    private fb: FormBuilder,
    private feedbackservice: FeedbackService
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: [
        "",
        [Validators.required, Validators.minLength(3), Validators.maxLength(25)]
      ],
      lastname: [
        "",
        [Validators.required, Validators.minLength(3), Validators.maxLength(25)]
      ],
      telnum: ["", [Validators.required, Validators.pattern]],
      email: ["", [Validators.required, Validators.email]],
      agree: false,
      contacttype: "None",
      message: "",
      date: Date.now()
    });

    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) {
      return;
    }
    const form = this.feedbackForm;

    // tslint:disable-next-line:forin
    for (const field in this.formErrors) {
      this.formErrors[field] = "";
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        // tslint:disable-next-line:forin
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + " ";
        }
      }
    }
  }

  onSubmit() {
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    this.showForm = false;
    this.feedbackservice.submitFeedback(this.feedback).then(
      feedback => {
        this.submitted = feedback;
        this.feedback = null;
        setTimeout(() => {
          this.submitted = null;
          this.showForm = true;
        }, 5000);
      },
      error => console.log(error.status, error.message)
    );
    this.feedbackForm.reset({
      firstname: "",
      lastname: "",
      telnum: "",
      email: "",
      agree: false,
      contacttype: "None",
      message: ""
    });
    this.feedbackFormDirective.resetForm();
  }
}
