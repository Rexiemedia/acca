import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-social",
  templateUrl: "./social.component.html",
  styleUrls: ["./social.component.scss"]
})
export class SocialComponent implements OnInit {
  public days: number;
  public hours: number;
  public mins: number;
  public seconds: number;
  notLunched = true;
  lunched = false;
  constructor() { }

  ngOnInit() {
    // const countdown = document.querySelector('.countdown');

    // Set Launch Date (ms)
    const launchDate = new Date("October 30, 2018 13:00:00").getTime();

    // Update every second
    const intvl = setInterval(() => {
      // Get todays date and time (ms)
      const now = new Date().getTime();

      // Distance from now and the launch date (ms)
      const distance = launchDate - now;

      // Time calculation
      this.days = Math.floor(distance / (1000 * 60 * 60 * 24));
      this.hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      this.mins = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      this.seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // // Display result
      // countdown.innerHTML = `
      // <div>${days}<span>Days</span></div>
      // <div>${hours}<span>Hours</span></div>
      // <div>${mins}<span>Minutes</span></div>
      // <div>${seconds}<span>Seconds</span></div>
      // `;

      // If launch date is reached
      if (distance < 0) {
        // Stop countdown
        clearInterval(intvl);
        // Style and output text
        //countdown.style.color = '#17a2b8';
        // countdown.innerHTML = 'Launched!';
        this.notLunched = false;
        this.lunched = true;
      }
    }, 1000);
  }
}
