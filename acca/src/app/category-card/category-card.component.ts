import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.scss']
})
export class CategoryCardComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }

  navigate(path) {
    this.router.navigate([{ outlets: { primary: path, sidemenu: path } }],
      { relativeTo: this.route });
  }

  ngOnInit() {
  }

}
