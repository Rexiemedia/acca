export class Sermon {
  _id?;
  title: string;
  message: string;
  author: string;
  featured: false;
}

export class Testimony {
  _id?;
  about: string;
  author: string;
}
