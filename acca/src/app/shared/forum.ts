export class Health {
  _id?;
  title?: string;
  message?: string;
  author?: string;
  featured: false;
}

export class Social {
    _id?;
    title?: string;
    message?: string;
    author?: string;
    featured: false;
}
