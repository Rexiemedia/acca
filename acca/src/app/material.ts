import {
    MatToolbarModule, MatButtonModule, MatSnackBarModule,
    MatSidenavModule, MatIconModule, MatListModule, MatTableModule,
    MatPaginatorModule, MatSortModule, MatCheckboxModule, MatFormFieldModule,
  MatInputModule, MatSlideToggleModule, 
} from '@angular/material';
import { NgModule } from '../../node_modules/@angular/core';


@NgModule({
  imports: [
    MatToolbarModule,
    MatButtonModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    
  ],
  exports: [
    MatToolbarModule,
    MatButtonModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    
  ]
})
export class MaterialModule {}
