import { BrowserModule  } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from './material';
import { MatDialogModule } from '@angular/material';
import { ServiceWorkerModule, SwUpdate, SwPush } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { MatSnackBar } from '../../node_modules/@angular/material';
import { AgmCoreModule } from '@agm/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { SermonComponent } from './sermon/sermon.component';
import { CategoryComponent } from './category/category.component';
import { SermonDetailsComponent } from './sermon-details/sermon-details.component';
import { HealthComponent } from './health/health.component';
import { HealthDetailsComponent } from './health-details/health-details.component';
import { AboutComponent } from './about/about.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { CategoryCardComponent } from './category-card/category-card.component';
import { CategorylistComponent } from './categorylist/categorylist.component';
import { HighlightDirective } from './highlight.directive';


import { FeedbackService } from './service/feedback.service';
import { PrayerService } from './service/prayer.service';
import { SermonService } from './service/sermon.service';
import { ForumService } from './service/forum.service';
import { AuthService } from './service/auth.service';
import { SocialComponent } from './social/social.component';
import { SocialDetailComponent } from './social-detail/social-detail.component';
import { LoginComponent } from './login/login.component';
import { PrayerComponent } from './prayer/prayer.component';
import { ShareModule } from '@ngx-share/core';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    ContactComponent,
    SermonComponent,
    CategoryComponent,
    SermonDetailsComponent,
    HealthComponent,
    HealthDetailsComponent,
    AboutComponent,
    SideMenuComponent,
    CategoryCardComponent,
    CategorylistComponent,
    HighlightDirective,
    SocialComponent,
    SocialDetailComponent,
    LoginComponent,
    PrayerComponent
  ],
  imports: [
    BrowserModule.withServerTransition({
      appId: 'Anointed Celebrating Christ Assembly'
    }),
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production
    }),
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_KEY' // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    HttpClientModule, // (Required) for share counts
    ShareModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [LoginComponent, PrayerComponent],
  providers: [
    PrayerService,
    FeedbackService,
    SermonService,
    ForumService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(update: SwUpdate, push: SwPush, snackbar: MatSnackBar) {
    update.available.subscribe(_update => {
      console.log("update available");
      const snack = snackbar.open("Update Available", "Reload");
      snack.onAction().subscribe(() => {
        window.location.reload();
      });
    });
    push.messages.subscribe(msg => {
      snackbar.open(JSON.stringify(msg));
    });
    const key =
      "BBMU5tegZZotwQ8L7Bq9ZKCgE-oAug_n3_BW2A_2WEariLVGTPb0j79iWmi2Sw23d5wK_E_rl2LCFYExDuEHS9M";
    push
      .requestSubscription({ serverPublicKey: key })
      .then(PushSubscription => {
        PushSubscription.toJSON();
        console.log("PushSubscription");
      });
  }
}
