import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
// import { MatDialog, MatDialogRef } from '../material';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AuthService } from '../service/auth.service';
import { LoginComponent } from '../login/login.component';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  username: string = undefined;
  avatar: any;

  constructor(
    private breakpointObserver: BreakpointObserver,
    public dialog: MatDialog,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        // User is signed in.
        // console.log('Logged In ', user.email);
        this.avatar = user.photoURL;
        // console.log(this.avatar);
        this.username = user.displayName ? user.displayName : user.email;
      } else {
        // console.log('Not Logged In');
        this.username = undefined;
      }
    });
  }

  openLoginForm() {
    this.dialog.open(LoginComponent, { width: 'auto', height: 'auto' });
  }
  logOut() {
    this.username = undefined;
    this.authService.logOut();
  }
}
