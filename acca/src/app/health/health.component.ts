import { Component, OnInit } from '@angular/core';
import { ForumService} from '../service/forum.service';
import { SeoService } from '../service/seo.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: "app-health",
  templateUrl: "./health.component.html",
  styleUrls: ["./health.component.scss"]
})
export class HealthComponent implements OnInit {
  public text;
  public hours: number;
  notLunched = true;

  public health: Object;
  errMess: string;

  constructor(
    private forumservice: ForumService,
    private titleService: Title,
    private meta: Meta,
    private seo: SeoService
  ) {}

  ngOnInit() {
    this.forumservice.getHealths().subscribe(healths => {
      Object.keys(healths).forEach(
        key => {
          var topics = healths[key];
          this.health = topics.Topics;
        },
        errmess => (this.errMess = errmess)
      );
    });

    this.seo.generateTags({
      title: 'Anointed Celebrating Christ Assembly',
      description:
        // tslint:disable-next-line:max-line-length
        'Health Tips: Health tips for healthy living. Do you know It is easy to get the folic acid you need?. Just eat a bowl of cereal with 100% DV of folic acid &ndash; or take a vitamin with 100% DV of folic acid &ndash; every day.',
      image: '',
      slug: 'health-page'
    });

    this.titleService.setTitle(this.text);
    this.meta.addTag({
      name: 'Health Tips',
      // tslint:disable-next-line:max-line-length
      content:
        // tslint:disable-next-line:max-line-length
        'Health tips for healthy living. Do you know It is easy to get the folic acid you need?. Just eat a bowl of cereal with 100% DV of folic acid &ndash; or take a vitamin with 100% DV of folic acid &ndash; every day.'
    });
  }
}
