import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { flyInOut, expand } from '../animations/app.animation';
import { PrayerComponent } from '../prayer/prayer.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[@flyInOut]': 'true',
    style: 'display: block;'
  },
  animations: [flyInOut(), expand()]
})
export class HomeComponent implements OnInit {
  constructor(public dialog: MatDialog) { }

  ngOnInit() { }

  openPrayerForm() {
    this.dialog.open(PrayerComponent, { width: 'auto', height: 'auto' });
  }
}
