import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { flyInOut, expand } from '../animations/app.animation';
import { Prayer } from '../shared/feedback';
import { PrayerService } from '../service/prayer.service';

@Component({
  selector: "app-prayer",
  templateUrl: "./prayer.component.html",
  styleUrls: ["./prayer.component.scss"],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    "[@flyInOut]": "true",
    style: "display: block;"
  },
  animations: [flyInOut(), expand()]
})
export class PrayerComponent implements OnInit {
  @ViewChild("fform")
  prayerFormDirective;
  prayerForm: FormGroup;
  prayer: Prayer;
  submitted = null;
  showForm = true;

  formErrors = {
    firstname: "",
    lastname: "",
    telnum: "",
    email: ""
  };

  validationMessages = {
    firstname: {
      required: "First Name is required.",
      minlength: "First Name must be at least 3 characters long",
      maxlength: "First Name cannot be more than 25 characters long"
    },
    lastname: {
      required: "Last Name is required.",
      minlength: "Last Name must be at least 3 characters long",
      maxlength: "Last Name cannot be more than 25 characters long"
    },
    telnum: {
      required: "Tel. Number is required.",
      pattern: "Tel. number must contain only numbers."
    },
    email: {
      required: "Email is required.",
      email: "Email not in valid format"
    }
  };

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<PrayerComponent>,
    private prayerservice: PrayerService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.prayerForm = this.fb.group({
      firstname: [
        "",
        [Validators.required, Validators.minLength(3), Validators.maxLength(25)]
      ],
      lastname: [
        "",
        [Validators.required, Validators.minLength(3), Validators.maxLength(25)]
      ],
      telnum: ["", [Validators.required, Validators.pattern]],
      email: ["", [Validators.required, Validators.email]],
      message: "",
      date: Date.now()
    });

    this.prayerForm.valueChanges.subscribe(data =>
      this.onValueChanged(data)
    );

    this.onValueChanged(); // (re)set form validation messages
  }

  onValueChanged(data?: any) {
    if (!this.prayerForm) {
      return;
    }
    const form = this.prayerForm;

    // tslint:disable-next-line:forin
    for (const field in this.formErrors) {
      this.formErrors[field] = "";
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        // tslint:disable-next-line:forin
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + " ";
        }
      }
    }
  }

  onSubmit() {
    this.prayer = this.prayerForm.value;
    this.showForm = false;
    this.prayerservice.submitPrayer(this.prayer).then(
      prayer => {
        this.submitted = prayer;
        this.prayer = null;
        setTimeout(() => {
          this.submitted = null;
          this.showForm = true;
        }, 5000);
      },
      error => console.log(error.status, error.message)
    );
    this.prayerForm.reset({
      firstname: "",
      lastname: "",
      telnum: "",
      email: "",
      message: ""
    });
    this.prayerFormDirective.resetForm();
    this.dialogRef.close();
  }
}
