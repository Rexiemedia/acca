import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { CategoryComponent } from './category/category.component';
import { CategoryCardComponent } from './category-card/category-card.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { CategorylistComponent } from './categorylist/categorylist.component';
import { HealthComponent } from './health/health.component';
import { HealthDetailsComponent } from './health-details/health-details.component';
import { SermonComponent } from './sermon/sermon.component';
import { SermonDetailsComponent } from './sermon-details/sermon-details.component';
import { SocialComponent } from './social/social.component';
import { SocialDetailComponent } from './social-detail/social-detail.component';

export const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'aboutus', component: AboutComponent },
    { path: 'contactus', component: ContactComponent },
    {
        path: 'category', component: CategoryComponent,
        children: [
            {
                path: '',
                component: CategoryCardComponent
            },
            {
                path: '',
                component: CategorylistComponent,
                children: [
                    {
                        path: 'sermon',
                        component: SermonComponent,
                    },
                    {
                        path: 'health',
                        component: HealthComponent,
                    },
                    {
                        path: 'social',
                        component: SocialComponent,
                    },
                    {
                        path: 'sermon/:id',
                        component: SermonDetailsComponent,
                    },
                    {
                        path: 'health/:id',
                        component: HealthDetailsComponent,
                    },
                    {
                        path: 'social/:id',
                        component: SocialDetailComponent,
                    }
                ]
            },
            {
                path: '',
                outlet: 'sidemenu',
                component: SideMenuComponent
            },
            {
                path: ':id',
                outlet: 'sidemenu',
                component: SideMenuComponent
            }

        ]
    },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '**', redirectTo: '/home', pathMatch: 'full' }
];
// https://s3-us-west-1.amazonaws.com/angular-university/mailing-list-downloads/welcome/Angular_Router_Crash_Course.pdf
